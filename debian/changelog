libclass-base-perl (0.09-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Sun, 22 Jan 2023 23:54:32 +0100

libclass-base-perl (0.09-1) unstable; urgency=medium

  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!
  * Import upstream version 0.09.
  * Update debian/upstream/metadata.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.1.3.
  * Bump debhelper compatibility level to 10.
  * Drop 0001-spelling-error-in-manpage.patch, merged upstream.
  * Add (build) dependency on libclone-perl.

 -- gregor herrmann <gregoa@debian.org>  Fri, 12 Jan 2018 19:15:00 +0100

libclass-base-perl (0.08-1) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Harlan Lieberman-Berg ]
  * New upstream release.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Nick Morrott ]
  * Imported Upstream version 0.08
  * Add debian/upstream/metadata
  * Bump Standards-Version to 3.9.8
  * Bump debhelper compatibility to 9
  * Update upstream contact
  * Reformat Uploaders list
  * Add Testsuite header to make package autopkgtest-able
  * Add 0001-spelling-error-in-manpage.patch

 -- Nick Morrott <knowledgejunkie@gmail.com>  Fri, 29 Jul 2016 16:10:05 +0100

libclass-base-perl (0.05-1) unstable; urgency=low

  * Imported Upstream version 0.05
  * Bump Standards-Version to 3.9.3
  * debian/copyright: update to Copyright-Format 1.0.

 -- Fabrizio Regalli <fabreg@fabreg.it>  Thu, 24 May 2012 00:15:17 +0200

libclass-base-perl (0.04-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  *  debian/control: Convert Vcs-* fields to Git.

  [ Fabrizio Regalli ]
  * Imported Upstream version 0.04
  * Updated d/compat to 8
  * Updated debhelper to (>= 8)
  * Added myself to Uploaders and Copyright
  * Bump Standards-Version to 3.9.2 (no changes needed)
  * Updated d/copyright using latest .174 DEP5 format
  * Switch to dpkg-source 3.0 (quilt) format

  [ gregor herrmann]
  * Update years of upstream copyright.

 -- Fabrizio Regalli <fabreg@fabreg.it>  Sun, 12 Feb 2012 13:38:19 +0100

libclass-base-perl (0.03-4) unstable; urgency=low

  * Take over for the Debian Perl Group with maintainer's permission
    (http://lists.debian.org/debian-perl/2009/10/msg00004.html)
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza);
    ${misc:Depends} to Depends: field. Changed: Maintainer set to Debian
    Perl Group <pkg-perl-maintainers@lists.alioth.debian.org> (was: Eric
    Dorland <eric@debian.org>); Eric Dorland <eric@debian.org> moved to
    Uploaders.
  * debian/watch: use dist-based URL.
  * Switch from cdbs to debhelper 7.
  * New format for debian/copyright.
  * Set Standards-Version to 3.8.3 (no changes).
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Thu, 08 Oct 2009 21:21:09 +0200

libclass-base-perl (0.03-3) unstable; urgency=low

  * debian/watch: Add watch file.
  * debian/control:
    - Standards-Version to 3.7.2.0.
    - Use Build-Depends instead of Build-Depends-Indep for things
      that need to be there for the clean target.

 -- Eric Dorland <eric@debian.org>  Thu, 15 Jun 2006 22:13:26 -0400

libclass-base-perl (0.03-2) unstable; urgency=low

  * debian/control:
    - Use Build-Depends-Indep instead of Build-Depends
    - Up Standards-Version to 3.6.2.2.
  * debian/compat: Added at 4 to make lintian shut up.

 -- Eric Dorland <eric@debian.org>  Thu, 23 Feb 2006 21:18:47 -0500

libclass-base-perl (0.03-1) unstable; urgency=low

  * Initial release.

 -- Eric Dorland <eric@debian.org>  Thu, 23 Feb 2006 15:41:53 -0500
